-- SUMMARY --

Allows you to save user roles per domain.
Allows a certain user to be just authenticated on one domain and
admin on another etc.

-- REQUIREMENTS --

domain Module-http://drupal.org/project/domain (Contrib Module) must be enabled.


-- INSTALLATION --

See the installation guide here - http://drupal.org/node/70151
